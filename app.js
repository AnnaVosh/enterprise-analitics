var express = require('express');
var path = require('path');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var moment = require('moment');
moment().format();
moment.locale('ru');

var app = express();

//указываем директории со статичными файлами (чтобы можно было обращаться к ним, не казывая путь, а просто по имени)
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'js')));
app.use(express.static(path.join(__dirname, 'views')));

app.use(bodyParser.json());                         // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//описываем модели и компилируем схемы на основе моделей
var companiesSchema = mongoose.Schema({
    title       : String,
    address     : String,
    head        : String,
    website     : String
});
companiesSchema.methods.print = function () {
    console.log('Название предприятия: '+ this.title +
                ' Адрес: ' + this.address +
                ' Руководитель: ' + this.head +
                ' Сайт: ' + this.website);
};
var Companies = mongoose.model('Companies', companiesSchema);

var meteringsSchema = mongoose.Schema({
    company_id      : String,
    year            : String,
    oldBalance      : Boolean,
    balanceValues   : [],
    gainsValues     : []
});
meteringsSchema.methods.print = function () {
    console.log(this);
};
var Meterings = mongoose.model('Meterings', meteringsSchema);

//массивы начальных данных
var companiesInitData = [
    {
        title       : 'ООО \"НИКА\"',
        address     : 'г. Москва, ул.Южнобутовская, д.45, пом.2',
        head        : 'Катулина Юлианна Владиславовна',
        website     : 'mail@titantorg.ru'
    },
    {
        title       : 'ООО \"ОАО Теплоизоляция\"',
        address     : 'г. Уфа, ул. Борисоглебская, д. 12',
        head        : 'Сагитов Альберт Ниилович',
        website     : 'teplo-miki.ru'
    }
];

var nikaCompanyInitData = [
    {
        company_id      : '',
        year            : '2005',
        oldBalance      : true,
        balanceValues   : [
            {
                val : 0,
                number : "120",
                "title" : "Основные средства"
            },
            {
                val : 0,
                number : "130",
                title : "Незавершенное строительство"
            },
            {
                val : 0,
                number : "135",
                title : "Доходные вложения в материальные ценности"
            },
            {
                val : 2532,
                number : "190",
                title : "Внеоборотные активы"
            },
            {
                val : 4274,
                number : "290",
                title : "Оборотные"
            },
            {
                val : 3747,
                number : "210",
                title : "Запасы"
            },
            {
                val : 86,
                number : "220",
                title : "НДС по приобретённым ценностям"
            },
            {
                val : 0,
                number : "230",
                title : "Дебитор задолженность (свыше 12м)"
            },
            {
                val : 15,
                number : "240",
                title : "Дебитор задолженность (менее 12м)"
            },
            {
                val : 0,
                number : "244",
                title : "Задолженность участников (учредителей) по взносам в УК"
            },
            {
                val : 0,
                number : "260",
                title : "Денежные средства"
            },
            {
                val : 0,
                number : "270",
                title : "Прочие оборотные активы"
            },
            {
                val : 1542,
                number : "490",
                title : "Капитал и резервы"
            },
            {
                val : 0,
                number : "410",
                title : "Уставной капитал"
            },
            {
                val : 0,
                number : "450",
                title : "Целевые финансирование и поступления"
            },
            {
                val : 0,
                number : "470",
                title : "Нераспределённая прибыль, непокрытый убыток"
            },
            {
                val : 0,
                number : "590",
                title : "Долгосрочные обязательства"
            },
            {
                val : 0,
                number : "510",
                title : "Займы и кредиты"
            },
            {
                val : 0,
                number : "520",
                title : "Прочие долгосрочные обязательства"
            },
            {
                val : 5264,
                number : "690",
                title : "Краткосрочные обязательства"
            },
            {
                val : 4395,
                number : "610",
                title : "Займы и кредиты"
            },
            {
                val : 869,
                number : "620",
                title : "Кредиторская задолженность"
            },
            {
                val : 0,
                number : "630",
                title : "Задолженность участникам"
            },
            {
                val : 0,
                number : "640",
                title : "Доходы будущих периодов"
            },
            {
                val : 0,
                number : "650",
                title : "Резервы предстоящих расходов"
            },
            {
                val : 0,
                number : "660",
                title : "Прочие краткосрочные обязательства"
            },
            {
                val : 6806,
                number : "300",
                title : "Всего активы"
            },
            {
                val : 6806,
                number : "700",
                title : "Всего пассивы"
            }
        ],
        gainsValues     : [
            {
                val : 18951,
                number : "010",
                title : "Выручка (нетто) от продажи товаров, продукции (за минусом НДС, акцизов)"
            },
            {
                val : -14557,
                number : "020",
                title : "Себестоимость проданных товаров, работ, услуг"
            },
            {
                val : 4394,
                number : "029",
                title : "Валовая прибыль"
            },
            {
                val : -3529,
                number : "030",
                title : "Коммерческие расходы"
            },
            {
                val : 0,
                number : "040",
                title : "Управленческие расходы"
            },
            {
                val : 865,
                number : "050",
                title : "Прибыль (убыток) от продаж"
            },
            {
                val : 491556,
                number : "070",
                title : "Проценты к уплате"
            },
            {
                val : 32795407,
                number : "100",
                title : "Прочие расходы"
            },
            {
                val : 0,
                number : "120",
                title : "Внереализационные доходы"
            },
            {
                val : 0,
                number : "130",
                title : "Внереализационные расходы"
            },
            {
                val : 318,
                number : "140",
                title : "Прибыль (убыток) до налогообложения"
            },
            {
                val : -76,
                number : "150",
                title : "Налог на прибыль"
            },
            {
                val : 0,
                number : "160",
                title : "Прибыль (убыток) от обычной деятельности"
            },
            {
                val : 0,
                number : "170",
                title : "Чрезвычайные доходы"
            },
            {
                val : 0,
                number : "180",
                title : "Чрезвычайные расходы"
            },
            {
                val : 242,
                number : "190",
                title : "Чистая прибыль"
            }
        ]
    },
    {
        company_id      : '',
        year            : '2006',
        oldBalance      : true,
        balanceValues   : [
            {
                val : 0,
                number : "120",
                "title" : "Основные средства"
            },
            {
                val : 0,
                number : "130",
                title : "Незавершенное строительство"
            },
            {
                val : 0,
                number : "135",
                title : "Доходные вложения в материальные ценности"
            },
            {
                val : 4047,
                number : "190",
                title : "Внеоборотные активы"
            },
            {
                val : 6800,
                number : "290",
                title : "Оборотные"
            },
            {
                val : 5857,
                number : "210",
                title : "Запасы"
            },
            {
                val : 42,
                number : "220",
                title : "НДС по приобретённым ценностям"
            },
            {
                val : 0,
                number : "230",
                title : "Дебитор задолженность (свыше 12м)"
            },
            {
                val : 840,
                number : "240",
                title : "Дебитор задолженность (менее 12м)"
            },
            {
                val : 0,
                number : "244",
                title : "Задолженность участников (учредителей) по взносам в УК"
            },
            {
                val : 0,
                number : "260",
                title : "Денежные средства"
            },
            {
                val : 0,
                number : "270",
                title : "Прочие оборотные активы"
            },
            {
                val : 2583,
                number : "490",
                title : "Капитал и резервы"
            },
            {
                val : 0,
                number : "410",
                title : "Уставной капитал"
            },
            {
                val : 0,
                number : "450",
                title : "Целевые финансирование и поступления"
            },
            {
                val : 0,
                number : "470",
                title : "Нераспределённая прибыль, непокрытый убыток"
            },
            {
                val : 2801,
                number : "590",
                title : "Долгосрочные обязательства"
            },
            {
                val : 2801,
                number : "510",
                title : "Займы и кредиты"
            },
            {
                val : 0,
                number : "520",
                title : "Прочие долгосрочные обязательства"
            },
            {
                val : 4992,
                number : "610",
                title : "Займы и кредиты"
            },
            {
                val : 471,
                number : "620",
                title : "Кредиторская задолженность"
            },
            {
                val : 0,
                number : "630",
                title : "Задолженность участникам"
            },
            {
                val : 0,
                number : "640",
                title : "Доходы будущих периодов"
            },
            {
                val : 0,
                number : "650",
                title : "Резервы предстоящих расходов"
            },
            {
                val : 0,
                number : "660",
                title : "Прочие краткосрочные обязательства"
            },
            {
                val : 5463,
                number : "690",
                title : "Краткосрочные обязательства"
            },
            {
                val : 10847,
                number : "300",
                title : "Всего активы"
            },
            {
                val : 10847,
                number : "700",
                title : "Всего пассивы"
            }
        ],
        gainsValues     : [
            {
                val : 10095,
                number : "010",
                title : "Выручка (нетто) от продажи товаров, продукции (за минусом НДС, акцизов)"
            },
            {
                val : -6725,
                number : "020",
                title : "Себестоимость проданных товаров, работ, услуг"
            },
            {
                val : 3370,
                number : "029",
                title : "Валовая прибыль"
            },
            {
                val : -3144,
                number : "030",
                title : "Коммерческие расходы"
            },
            {
                val : 0,
                number : "040",
                title : "Управленческие расходы"
            },
            {
                val : 226,
                number : "050",
                title : "Прибыль (убыток) от продаж"
            },
            {
                val : 2399560,
                number : "070",
                title : "Проценты к уплате"
            },
            {
                val : 70824796,
                number : "100",
                title : "Прочие расходы"
            },
            {
                val : 0,
                number : "120",
                title : "Внереализационные доходы"
            },
            {
                val : 0,
                number : "130",
                title : "Внереализационные расходы"
            },
            {
                val : 388,
                number : "140",
                title : "Прибыль (убыток) до налогообложения"
            },
            {
                val : -76,
                number : "150",
                title : "Налог на прибыль"
            },
            {
                val : 0,
                number : "160",
                title : "Прибыль (убыток) от обычной деятельности"
            },
            {
                val : 0,
                number : "170",
                title : "Чрезвычайные доходы"
            },
            {
                val : 0,
                number : "180",
                title : "Чрезвычайные расходы"
            },
            {
                val : 388,
                number : "190",
                title : "Чистая прибыль"
            }
        ]
    },
    {
        company_id      : '',
        year            : '2007',
        oldBalance      : true,
        balanceValues   : [
            {
                val : 0,
                number : "120",
                "title" : "Основные средства"
            },
            {
                val : 0,
                number : "130",
                title : "Незавершенное строительство"
            },
            {
                val : 0,
                number : "135",
                title : "Доходные вложения в материальные ценности"
            },
            {
                val : 5258,
                number : "190",
                title : "Внеоборотные активы"
            },
            {
                val : 8174,
                number : "290",
                title : "Оборотные"
            },
            {
                val : 7251,
                number : "210",
                title : "Запасы"
            },
            {
                val : 56,
                number : "220",
                title : "НДС по приобретённым ценностям"
            },
            {
                val : 0,
                number : "230",
                title : "Дебитор задолженность (свыше 12м)"
            },
            {
                val : 704,
                number : "240",
                title : "Дебитор задолженность (менее 12м)"
            },
            {
                val : 0,
                number : "244",
                title : "Задолженность участников (учредителей) по взносам в УК"
            },
            {
                val : 0,
                number : "260",
                title : "Денежные средства"
            },
            {
                val : 0,
                number : "270",
                title : "Прочие оборотные активы"
            },
            {
                val : 3613,
                number : "490",
                title : "Капитал и резервы"
            },
            {
                val : 0,
                number : "410",
                title : "Уставной капитал"
            },
            {
                val : 0,
                number : "450",
                title : "Целевые финансирование и поступления"
            },
            {
                val : 0,
                number : "470",
                title : "Нераспределённая прибыль, непокрытый убыток"
            },
            {
                val : 623,
                number : "590",
                title : "Долгосрочные обязательства"
            },
            {
                val : 623,
                number : "510",
                title : "Займы и кредиты"
            },
            {
                val : 0,
                number : "520",
                title : "Прочие долгосрочные обязательства"
            },
            {
                val : 8192,
                number : "610",
                title : "Займы и кредиты"
            },
            {
                val : 1004,
                number : "620",
                title : "Кредиторская задолженность"
            },
            {
                val : 0,
                number : "630",
                title : "Задолженность участникам"
            },
            {
                val : 0,
                number : "640",
                title : "Доходы будущих периодов"
            },
            {
                val : 0,
                number : "650",
                title : "Резервы предстоящих расходов"
            },
            {
                val : 0,
                number : "660",
                title : "Прочие краткосрочные обязательства"
            },
            {
                val : 9196,
                number : "690",
                title : "Краткосрочные обязательства"
            },
            {
                val : 13432,
                number : "300",
                title : "Всего активы"
            },
            {
                val : 13432,
                number : "700",
                title : "Всего пассивы"
            }
        ],
        gainsValues     : [
            {
                val : 21858,
                number : "010",
                title : "Выручка (нетто) от продажи товаров, продукции (за минусом НДС, акцизов)"
            },
            {
                val : -16287,
                number : "020",
                title : "Себестоимость проданных товаров, работ, услуг"
            },
            {
                val : 5571,
                number : "029",
                title : "Валовая прибыль"
            },
            {
                val : 3541,
                number : "030",
                title : "Коммерческие расходы"
            },
            {
                val : 0,
                number : "040",
                title : "Управленческие расходы"
            },
            {
                val : 1120,
                number : "050",
                title : "Прибыль (убыток) от продаж"
            },
            {
                val : 2464042,
                number : "070",
                title : "Проценты к уплате"
            },
            {
                val : 60082250,
                number : "100",
                title : "Прочие расходы"
            },
            {
                val : 0,
                number : "120",
                title : "Внереализационные доходы"
            },
            {
                val : 0,
                number : "130",
                title : "Внереализационные расходы"
            },
            {
                val : 1272,
                number : "140",
                title : "Прибыль (убыток) до налогообложения"
            },
            {
                val : -340,
                number : "150",
                title : "Налог на прибыль"
            },
            {
                val : 0,
                number : "160",
                title : "Прибыль (убыток) от обычной деятельности"
            },
            {
                val : 0,
                number : "170",
                title : "Чрезвычайные доходы"
            },
            {
                val : 0,
                number : "180",
                title : "Чрезвычайные расходы"
            },
            {
                val : 932,
                number : "190",
                title : "Чистая прибыль"
            }
        ]
    },
    {
        company_id      : '',
        year            : '2008',
        oldBalance      : true,
        balanceValues   : [
            {
                val : 5125,
                number : "120",
                "title" : "Основные средства"
            },
            {
                val : 0,
                number : "130",
                title : "Незавершенное строительство"
            },
            {
                val : 0,
                number : "135",
                title : "Доходные вложения в материальные ценности"
            },
            {
                val : 6345,
                number : "190",
                title : "Внеоборотные активы"
            },
            {
                val : 10770,
                number : "290",
                title : "Оборотные"
            },
            {
                val : 9510,
                number : "210",
                title : "Запасы"
            },
            {
                val : 36,
                number : "216",
                title : "Расходы будущих периодов"
            },
            {
                val : 20,
                number : "220",
                title : "НДС по приобретённым ценностям"
            },
            {
                val : 0,
                number : "230",
                title : "Дебитор задолженность (свыше 12м)"
            },
            {
                val : 1039,
                number : "240",
                title : "Дебитор задолженность (менее 12м)"
            },
            {
                val : 0,
                number : "244",
                title : "Задолженность участников (учредителей) по взносам в УК"
            },
            {
                val : 144,
                number : "260",
                title : "Денежные средства"
            },
            {
                val : 57,
                number : "270",
                title : "Прочие оборотные активы"
            },
            {
                val : 3288,
                number : "490",
                title : "Капитал и резервы"
            },
            {
                val : 10,
                number : "410",
                title : "Уставной капитал"
            },
            {
                val : 0,
                number : "450",
                title : "Целевые финансирование и поступления"
            },
            {
                val : 2825,
                number : "470",
                title : "Нераспределённая прибыль, непокрытый убыток"
            },
            {
                val : 4445,
                number : "590",
                title : "Долгосрочные обязательства"
            },
            {
                val : 4445,
                number : "510",
                title : "Займы и кредиты"
            },
            {
                val : 0,
                number : "520",
                title : "Прочие долгосрочные обязательства"
            },
            {
                val : 8969,
                number : "610",
                title : "Займы и кредиты"
            },
            {
                val : 413,
                number : "620",
                title : "Кредиторская задолженность"
            },
            {
                val : 0,
                number : "630",
                title : "Задолженность участникам"
            },
            {
                val : 0,
                number : "640",
                title : "Доходы будущих периодов"
            },
            {
                val : 0,
                number : "650",
                title : "Резервы предстоящих расходов"
            },
            {
                val : 0,
                number : "660",
                title : "Прочие краткосрочные обязательства"
            },
            {
                val : 9382,
                number : "690",
                title : "Краткосрочные обязательства"
            },
            {
                val : 17115,
                number : "300",
                title : "Всего активы"
            },
            {
                val : 17115,
                number : "700",
                title : "Всего пассивы"
            }
        ],
        gainsValues     : [
            {
                val : 16549,
                number : "010",
                title : "Выручка (нетто) от продажи товаров, продукции (за минусом НДС, акцизов)"
            },
            {
                val : 13493,
                number : "020",
                title : "Себестоимость проданных товаров, работ, услуг"
            },
            {
                val : 3056,
                number : "029",
                title : "Валовая прибыль"
            },
            {
                val : 2847,
                number : "030",
                title : "Коммерческие расходы"
            },
            {
                val : 0,
                number : "040",
                title : "Управленческие расходы"
            },
            {
                val : 96,
                number : "050",
                title : "Прибыль (убыток) от продаж"
            },
            {
                val : 0,
                number : "070",
                title : "Проценты к уплате"
            },
            {
                val : 101,
                number : "100",
                title : "Прочие расходы"
            },
            {
                val : 0,
                number : "120",
                title : "Внереализационные доходы"
            },
            {
                val : 0,
                number : "130",
                title : "Внереализационные расходы"
            },
            {
                val : 48,
                number : "140",
                title : "Прибыль (убыток) до налогообложения"
            },
            {
                val : 11,
                number : "150",
                title : "Налог на прибыль"
            },
            {
                val : 0,
                number : "160",
                title : "Прибыль (убыток) от обычной деятельности"
            },
            {
                val : 0,
                number : "170",
                title : "Чрезвычайные доходы"
            },
            {
                val : 0,
                number : "180",
                title : "Чрезвычайные расходы"
            },
            {
                val : 37,
                number : "190",
                title : "Чистая прибыль"
            }
        ]
    }
];

var teploisolationCompanyInitData = [
    {
        company_id      : '',
        year            : '2011',
        oldBalance      : false,
        balanceValues   : [
            {
                title : 'Внеоборотные активы (итого по разделу)',
                number: '1100',
                val   : 5100
            },
            {
                title : 'Оборотные активы (итого по разделу)',
                number: '1200',
                val   : 26649
            },
            {
                title : 'Запасы (оборотные активы)',
                number: '1210',
                val   : 12978
            },
            {
                title : 'НДС по приобретённым ценностям',
                number: '1220',
                val   : 0
            },
            {
                title : 'Дебеторскя задолженность',
                number: '1230',
                val   : 12729
            },
            {
                title : 'Всего активы (баланс)',
                number: '1600',
                val   : 31749
            },
            {
                title : 'Капитал и резервы (итого)',
                number: '1300',
                val   : 23458
            },
            {
                title : 'Долгосрочные обязательства (итого)',
                number: '1400',
                val   : 677
            },
            {
                title : 'Краткосрочные обязательства (итого)',
                number: '1500',
                val   : 7614
            },
            {
                title : 'Займы и кредиты',
                number: '1510',
                val   : 0
            },
            {
                title : 'Кредиторская задолженность',
                number: '1520',
                val   : 7141
            },
            {
                title : 'Доходы будующих периодов',
                number: '1530',
                val   : 0
            }
        ],
        gainsValues     : [
            {
                title : 'Выручка (нетто) от продажи товаров, продукции (за минусом НДС, акцизов)',
                number: '2110',
                val   : 63288
            },
            {
                title : 'Себестоимость проданных товаров, работ, услуг',
                number: '2120',
                val   : -61895
            },
            {
                title : 'Коммерческие расходы',
                number: '2210',
                val   : 0
            },
            {
                title : 'Управленческие расходы',
                number: '2220',
                val   : 0
            },
            {
                title : 'Прибыль (убыток) от продаж',
                number: '2200',
                val   : 1393
            },
            {
                title : 'Внереализационные доходы',
                number: '2340',
                val   : 9654
            },
            {
                title : 'Прибыль (убыток) до налогообложения',
                number: '2300',
                val   : 3113
            },
            {
                title : 'Налог на прибыль',
                number: '2410',
                val   : 0
            },
            {
                title : 'Чистая прибыль',
                number: '2400',
                val   : 3213
            }

        ]
    }
];

function removeAllData(model) {
    model.find({}).remove().exec();
}

function printAllData(model) {
    model.find(function (err, data) {
        if (err) return console.error(err);
        for (var i in data) {
            data[i].print();
        }
    });
}

function fillModel(model,data) {
    removeAllData(model);
    for (var i in data) {
        new model(data[i]).save(
            function(err) {
                if (err) return console.error(err);
            }
        )
    }
}

function addToModel(model,data) {
    for (var i in data) {
        new model(data[i]).save(
            function(err) {
                if (err) return console.error(err);
            }
        )
    }
}

function connectMeteringsToCompany(model,data) {
    model.findOne({},function(err,doc) {
        for (var i in data) {
            data[i].company_id = doc._id;
        }
        fillModel(Meterings,data);
    });
}

function createCompany(companyData, meterings) {
    new Companies(companyData).save(
        function(err,company) {
            if (err) return console.error(err);

            for (var i in meterings) {
                meterings[i].company_id = company._id;
            }

            addToModel(Meterings,meterings);
        }
    )
}

//стартуем сервер
app.listen(5000, function () {
    console.log('Enterprise Analitics слушает порт 5000!');
    //настраиваем базу данных
    mongoose.connect('mongodb://localhost/ea1');
    //откываем подключение
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function() {
        console.log('Подключение к базе успешно установлено.');
        // fillModel(Companies,companiesInitData);
        // connectMeteringsToCompany(Companies,meteringsInitData);
        removeAllData(Companies);
        removeAllData(Meterings);

        createCompany(companiesInitData[0],nikaCompanyInitData);
        createCompany(companiesInitData[1],teploisolationCompanyInitData);
    });
});

//главная страница
app.use(function (req, res, next) {
    if (req.url == '/') {
        res.sendFile(__dirname + '/views/index.html');
    } else {
        next();
    }
});

app.get('/get_all_companies', function (req, res) {
    Companies.find({},function(err,docs) {
        res.send(docs);
    })
});

app.post('/add_company', function (req, res) {
    new Companies(req.body).save(
        function(err) {
            if (err) return console.error(err);
        }
    );
    res.sendStatus(200);
});

app.put('/edit_company', function (req, res) {
    Companies.findOne({_id:req.body._id},function(err,doc) {
        doc.title = req.body.title;
        doc.address = req.body.address;
        doc.head = req.body.head;
        doc.website = req.body.website;
        doc.save(
            function(err) {
                if (err) return console.error(err);
                res.sendStatus(200);
            }
        )
    });
});

app.post('/del_company', function (req, res) {
    Companies.find({_id:req.body._id}).remove(() => {
        res.sendStatus(200);
    })
});

app.get('/get_company/:id', function (req, res) {
    Companies.findOne({_id:req.params.id},function(err,doc) {
        res.send(doc);
    })
});

app.post('/get_all_meterings', function (req, res) {
    Meterings.find({company_id:req.body.id}).sort('year').exec(function (err, docs) {
        res.send(docs);
    })
});

app.post('/add_metering', function (req, res) {
    new Meterings({
        company_id      : req.body.company_id,
        year            : req.body.year,
        oldBalance      : req.body.oldBalance,
        balanceValues   : req.body.balanceValues,
        gainsValues     : req.body.gainsValues
    }).save(
        function(err,item) {
            if (err) return console.error(err);
        }
    );
    res.sendStatus(200);
});

app.get('/get_meter/:id', function (req, res) {
    Meterings.findOne({_id:req.params.id},function(err,doc) {
        res.send(doc);
    })
});

//если поступает запрос на запрещенный для юзера адрес
app.use(function (req, res, next) {
    if (req.url == '/forbidden') {
        res.sendStatus(401); // извини, досвидания
    } else {
        next();
    }
});

//заглушка, теперь все левые запросы будут переадресовываться на гравную страницу
app.use(function (req, res) {
    res.sendFile(__dirname + '/views/index.html');
});

