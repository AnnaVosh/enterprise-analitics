// app.js
var routerApp = angular.module('routerApp', [
    'ui.router',
    'controllers',
    'moment_module',
    'chart.js'
]);

routerApp
    .config(
        function($stateProvider, $locationProvider, $urlRouterProvider) {
            $locationProvider.html5Mode({
              enabled:true,
              requireBase:true,
              rewriteLinks:true
            });
            
            $stateProvider
                .state('home', {
                    url: '/home',
                    templateUrl: 'home.html',
                    controller: 'HomeCtrl'
                })
                .state('companies', {
                    url: '/companies',
                    templateUrl: 'companies.html',
                    controller: 'CompaniesCtrl'
                })
                .state('company', {
                    url: '/company/:id',
                    templateUrl: 'company.html',
                    controller: 'CompanyCtrl'
                })
                .state('analitics', {
                    url: '/analitics',
                    templateUrl: 'analitics.html',
                    controller: 'AnaliticsCtrl'
                })
                .state('statistics', {
                    url: '/statistics',
                    templateUrl: 'statistics.html',
                    controller: 'StatisticsCtrl'
                })
        }
    )
    .run(
        ['$state', 
            function ($state) {
                $state.transitionTo('home');
            }
        ]
    );
