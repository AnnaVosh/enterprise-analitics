var controllers = angular.module('controllers', []);

controllers.controller('HomeCtrl',
    function ($scope,$rootScope, $http) {
        $rootScope.activePage='home';
    }
);
 
controllers.controller('CompaniesCtrl',
    function ($scope,$rootScope, $http, $state) {
        $rootScope.activePage='companies';
        $scope.paramsForAdd = {
            title   : '',
            address : '',
            head    : '',
            website : ''
        };
        $scope.selectedCompany = {
            
        };

        let needGoToCompany = false;
        
        function getData() {
            $http
                .get('/get_all_companies')
                .success(success)
                .error(error);

            function success(data) {
                $scope.items = data;
            }
            function error() {
                console.log('Error when get companies data');
            }
        }
        
        $scope.addCompany = () => {
            $http
                .post('/add_company',$scope.paramsForAdd)
                .success(success)
                .error(error);
            
                function success(data) {
                    getData();
                }
                function error() {
                    console.log('Error when add company data');
                }
        };

        $scope.editCompany = () => {
            $http
                .put('/edit_company',$scope.selectedCompany)
                .success(success)
                .error(error);

            function success(data) {
                $scope.selectPdkItem = {};
                $('#selectOptionModal').modal('hide');
                getData();
            }
            function error() {
                console.log('Error when edit company data');
            }
        };

        $scope.removeCompany = () => {
            console.log($scope.selectedCompany);
            $http
                .post('/del_company',$scope.selectedCompany)
                .success(success)
                .error(error);

            function success(data) {
                $scope.selectPdkItem = {};
                $('#selectOptionModal').modal('hide');
                getData();
            }
            function error() {
                console.log('Error when edit company data');
            }
        };

        $scope.goToCompany = () => {
            console.log($scope.selectedCompany._id);
            needGoToCompany = true;
            $('#selectOptionModal').modal('hide');
        };

        $scope.goConfig = (item) => {
            $scope.selectedCompany = item;
            $('#selectOptionModal').modal('show');
        };

        $('#selectOptionModal').on('hidden.bs.modal', function (e) {
            if (needGoToCompany) {
                $state.go('company',{
                    id:$scope.selectedCompany._id
                })
            }
        });
        
        //start
        getData();
    }
);

controllers.controller('CompanyCtrl',
    function ($scope, $rootScope, $state, $http) {
        $rootScope.activePage='no';
        $scope.newMetering = {
            company_id      : '',
            year            : '',
            oldBalance      : '?',
            balanceValues   : [],
            gainsValues     : []
        };
        $scope.selectedYear = 'Выберите год отчетного периода';
        $scope.readyToShow = false;

        let oldBalanceValues = [
            {
                title : 'Основные средства',
                number: '120',
                val   : 0
            },
            {
                title : 'Незавершенное строительство',
                number: '130',
                val   : 0
            },
            {
                title : 'Доходные вложения в материальные ценности',
                number: '135',
                val   : 0
            },
            {
                title : 'Внеоборотные активы',
                number: '190',
                val   : 0
            },
            {
                title : 'Оборотные',
                number: '290',
                val   : 0
            },
            {
                title : 'Запасы',
                number: '210',
                val   : 0
            },
            {
                title : 'НДС по приобретённым ценностям',
                number: '220',
                val   : 0
            },
            {
                title : 'Дебитор задолженность (свыше 12м)',
                number: '230',
                val   : 0
            },
            {
                title : 'Дебитор задолженность (менее 12м)',
                number: '240',
                val   : 0
            },
            {
                title : 'Задолженность участников (учредителей) по взносам в УК',
                number: '244',
                val   : 0
            },
            {
                title : 'Денежные средства',
                number: '260',
                val   : 0
            },
            {
                title : 'Прочие оборотные активы',
                number: '270',
                val   : 0
            },
            {
                title : 'Капитал и резервы',
                number: '490',
                val   : 0
            },
            {
                title : 'Уставной капитал',
                number: '410',
                val   : 0
            },
            {
                title : 'Целевые финансирование и поступления',
                number: '450',
                val   : 0
            },
            {
                title : 'Нераспределённая прибыль, непокрытый убыток',
                number: '470',
                val   : 0
            },
            {
                title : 'Долгосрочные обязательства',
                number: '590',
                val   : 0
            },
            {
                title : 'Займы и кредиты',
                number: '510',
                val   : 0
            },
            {
                title : 'Прочие долгосрочные обязательства',
                number: '520',
                val   : 0
            },
            {
                title : 'Краткосрочные обязательства',
                number: '690',
                val   : 0
            },
            {
                title : 'Займы и кредиты',
                number: '610',
                val   : 0
            },
            {
                title : 'Кредиторская задолженность',
                number: '620',
                val   : 0
            },
            {
                title : 'Задолженность участникам',
                number: '630',
                val   : 0
            },
            {
                title : 'Доходы будущих периодов',
                number: '640',
                val   : 0
            },
            {
                title : 'Резервы предстоящих расходов',
                number: '650',
                val   : 0
            },
            {
                title : 'Прочие краткосрочные обязательства',
                number: '660',
                val   : 0
            },
            {
                title : 'Краткосрочные обязательства',
                number: '690',
                val   : 0
            },
        ];
        let oldGainsValues = [
            {
                title : 'Выручка (нетто) от продажи товаров, продукции (за минусом НДС, акцизов)',
                number: '010',
                val   : 0
            },
            {
                title : 'Себестоимость проданных товаров, работ, услуг',
                number: '020',
                val   : 0
            },
            {
                title : 'Валовая прибыль',
                number: '029',
                val   : 0
            },
            {
                title : 'Коммерческие расходы',
                number: '030',
                val   : 0
            },
            {
                title : 'Управленческие расходы',
                number: '040',
                val   : 0
            },
            {
                title : 'Прибыль (убыток) от продаж',
                number: '050',
                val   : 0
            },
            {
                title : 'Проценты к уплате',
                number: '070',
                val   : 0
            },
            {
                title : 'Прочие расходы',
                number: '100',
                val   : 0
            },
            {
                title : 'Внереализационные доходы',
                number: '120',
                val   : 0
            },
            {
                title : 'Внереализационные расходы',
                number: '130',
                val   : 0
            },
            {
                title : 'Прибыль (убыток) до налогообложения',
                number: '140',
                val   : 0
            },
            {
                title : 'Налог на прибыль',
                number: '150',
                val   : 0
            },
            {
                title : 'Прибыль (убыток) от обычной деятельности',
                number: '160',
                val   : 0
            },
            {
                title : 'Чрезвычайные доходы',
                number: '170',
                val   : 0
            },
            {
                title : 'Чрезвычайные расходы',
                number: '180',
                val   : 0
            },
            {
                title : 'Чистая прибыль',
                number: '190',
                val   : 0
            }
        ];

        let newBalanceValues = [
            {
                title : 'Внеоборотные активы (итого по разделу)',
                number: '1100',
                val   : 0
            },
            {
                title : 'Оборотные активы (итого по разделу)',
                number: '1200',
                val   : 0
            },
            {
                title : 'Запасы (оборотные активы)',
                number: '1210',
                val   : 0
            },
            {
                title : 'НДС по приобретённым ценностям',
                number: '1220',
                val   : 0
            },
            {
                title : 'Дебеторскя задолженность',
                number: '1230',
                val   : 0
            },
            {
                title : 'Всего активы (баланс)',
                number: '1600',
                val   : 0
            },
            {
                title : 'Капитал и резервы (итого)',
                number: '1300',
                val   : 0
            },
            {
                title : 'Долгосрочные обязательства (итого)',
                number: '1400',
                val   : 0
            },
            {
                title : 'Краткосрочные обязательства (итого)',
                number: '1500',
                val   : 0
            },
            {
                title : 'Займы и кредиты',
                number: '1510',
                val   : 0
            },
            {
                title : 'Кредиторская задолженность',
                number: '1520',
                val   : 0
            },
            {
                title : 'Доходы будующих периодов',
                number: '1530',
                val   : 0
            }
        ];
        let newGainsValues = [
            {
                title : 'Выручка (нетто) от продажи товаров, продукции (за минусом НДС, акцизов)',
                number: '2110',
                val   : 0
            },
            {
                title : 'Себестоимость проданных товаров, работ, услуг',
                number: '2120',
                val   : 0
            },
            {
                title : 'Коммерческие расходы',
                number: '2210',
                val   : 0
            },
            {
                title : 'Управленческие расходы',
                number: '2220',
                val   : 0
            },
            {
                title : 'Прибыль (убыток) от продаж',
                number: '2200',
                val   : 0
            },
            {
                title : 'Внереализационные доходы',
                number: '2340',
                val   : 0
            },
            {
                title : 'Прибыль (убыток) до налогообложения',
                number: '2300',
                val   : 0
            },
            {
                title : 'Налог на прибыль',
                number: '2410',
                val   : 0
            },
            {
                title : 'Чистая прибыль',
                number: '2400',
                val   : 0
            }

        ];
        
        function getData() {
            $http
                .get('/get_company/'+$state.params.id)
                .success(success)
                .error(error);

            function success(data) {
                $scope.company = data;
                getAllMeterings();
            }
            function error() {
                console.log('Error when get pdk data');
            }
        }
        
        function getAllMeterings() {
            $http
                .post('/get_all_meterings',{id:$scope.company._id})
                .success(success)
                .error(error);

            function success(data) {
                $scope.meterings = data;
            }
            function error() {
                console.log('Error when get meterings data');
            }
        }
        
        $scope.addMetering = () => {
            console.log($scope.newMetering);
            $scope.newMetering.company_id = $scope.company._id;
            //переводим значения статей в числа
            for (let i in $scope.newMetering.balanceValues) {
                $scope.newMetering.balanceValues[i].val = parseInt($scope.newMetering.balanceValues[i].val);
            }
            for (let i in $scope.newMetering.gainsValues) {
                $scope.newMetering.gainsValues[i].val = parseInt($scope.newMetering.gainsValues[i].val);
            }

            if ($scope.newMetering.oldBalance) {
                //заполняем статьи 700 и 300 - всего пассивы и всего активы
                let act190;
                let act290;
                let pass590;
                let pass690;

                for (let i in $scope.newMetering.balanceValues) {
                    if ($scope.newMetering.balanceValues[i].number === '190') {
                        act190 = $scope.newMetering.balanceValues[i].val;
                    }
                    if ($scope.newMetering.balanceValues[i].number === '290') {
                        act290 = $scope.newMetering.balanceValues[i].val;
                    }
                    if ($scope.newMetering.balanceValues[i].number === '590') {
                        pass590 = $scope.newMetering.balanceValues[i].val;
                    }
                    if ($scope.newMetering.balanceValues[i].number === '690') {
                        pass690 = $scope.newMetering.balanceValues[i].val;
                    }
                }
                $scope.newMetering.balanceValues.push({
                    title : 'Всего активы',
                    number: '300',
                    val   : act190 + act290
                });
                $scope.newMetering.balanceValues.push({
                    title : 'Всего пассивы',
                    number: '700',
                    val   : pass590 + pass690
                });
            } else {
                //новый баланс
            }


            $http
                .post('/add_metering',$scope.newMetering)
                .success(success)
                .error(error);

            function success(data) {
                $scope.newMetering.gainsValues = [];
                $scope.newMetering.balanceValues = [];
                $scope.newMetering.oldBalance = '?';
                $scope.newMetering.year = '';
                getAllMeterings();
            }
            function error() {
                console.log('Error when add metering data');
            }
        };
        
        $scope.cancelAddMetering = () => {
            $scope.newMetering.gainsValues = [];
            $scope.newMetering.balanceValues = [];
            $scope.newMetering.oldBalance = '?';
            $scope.newMetering.year = '';
        };
        
        $scope.selectYear = (item) => {
            $scope.selectedYear = item.year;
            $scope.selectedMetering = item;
            $scope.readyToShow = true;
        };
        
        $scope.setBalanceType = (value) => {
            $scope.newMetering.oldBalance = value;
            if (value) {
                $scope.newMetering.balanceValues = oldBalanceValues;
                $scope.newMetering.gainsValues = oldGainsValues;
            } else {
                $scope.newMetering.balanceValues = newBalanceValues;
                $scope.newMetering.gainsValues = newGainsValues;
            }
        };
        
        $scope.goConfig = (item) => {
            console.log(item);
        };
        //start
        getData();
    }
);

controllers.controller('AnaliticsCtrl',
    function ($scope, $rootScope, $state, $http) {
        $rootScope.activePage='analitics';
        $scope.selectedCompany = {
            title:'Выберите предприятие'
        };
        $scope.selectedYear = 'Выберите год';
        $scope.readyToStartAnalisis = false;

        let defaultMeter;
        //Даные для дискриминантного анализа
        //OH	H	C	B	OB
        let yLP=[
            [0.01413, -0.01426, -0.18903, -0.06446, 0.10890],
            [0.00000, 0.00000, 0.00000, 0.00000, 0.00005],
            [-0.00669, 0.00701, 0.09173, 0.03752, -0.05100]
        ];
        let yLPConst=[-2.13161, -1.74392, -2.52403, -1.02722, -1.67347];
        
        //OH	H	C	B	OB
        let yA=[
            [-0.01094, 0.02234, 0.12056, 0.21359, 0.74670],
            [-0.00003, 0.00039, 0.00275, 0.02066, 0.00635],
            [0.00004, 0.00009, -0.00003, 0.00007, 0.00207],
            [0.00465, 0.01623, 0.02145, 0.03565, 0.12730]
        ];
        let yAConst=[-2.30314, -1.71750, -2.28467, -2.17907, -2.2480];

        //OH	H	C	B	OB
        let yF=[
            [7.6767, 5.7303, 3.8304, 2.9434, 2.8056],
            [21.4982, 26.7308, 33.3068, 40.4152, 53.0156],
            [-0.1167, -0.0608, -0.0189, -0.0014, 0.0118],
            [17.7457, 12.4431, 6.7760, 3.9053, 2.7348]
        ];
        let yFConst=[-39.2155, -23.4506, -15.1703, -16.4153, -25.3109];

        //OH	H	C	B	OB
        let yR=[
            [-0.00565, -0.01902, 0.00214, 0.00959, 0.03159],
            [0.18059, -0.05980, -0.01223, -0.08932, -0.00159],
            [-0.20744, 0.00622, 0.02700, 0.21887, 0.18724],
            [-0.04476, -0.00517, 0.02840, 0.07285, 0.18176]
        ];
        let yRConst=[-3.17056, -2.21235, -1.50504, -2.96261, -6.48610];

        //OH	H	C	B	OB
        let yAll=[
            [-0.2001, -0.1153, 0.0985, 0.2593, 0.6398],
            [0.0001, 0.0001, 0.0002, 0.0002, 0.0002],
            [0.0910, 0.0508, -0.0518, -0.1255, -0.3305],
            [7.0748, 8.8457, 4.8107, 4.0001, 4.0981],
            [36.0026, 44.2849, 40.1107, 43.3181, 51.1191],
            [0.1378, 0.1481, 0.0691, 0.0963, 0.0779],
            [12.0865, 12.4238, 7.6010, 6.7569, 6.1785],
            [0.4846, 0.6381, 1.0239, 1.7223, 1.9749],
            [-0.0049, -0.0081, -0.0077, -0.0071, 0.0144],
            [-0.0107, -0.0120, -0.0019, -0.0054, -0.0040],
            [-0.0972, -0.0905, 0.0058, -0.0387, -0.0242],
            [-0.0108, -0.0142, -0.0140, 0.0017, 0.0097],
            [0.2614, 0.3315, -0.0130, 0.0521, 0.1898],
            [-0.1947, -0.2227, 0.0380, 0.0909, 0.0272],
            [-0.1877, -0.1844, -0.0597, -0.0321, 0.0001]
        ];
        let yAllConst=[-29.8741, -37.1514, -18.7667, -19.7598, -27.4437];

        //нужны для диаграммы
        $scope.ratioLabels = ['Ликвидность', 'Фин. уст-ть', 'Деловая акт-ть', 'Рентабельность'];
        $scope.ratioSeries = ['Количественное значение', 'Качественное'];
        $scope.ratioData = [
            [1, 1, 1, 1],
            [1, 1, 1, 1]
        ];

        $scope.discriminantLabels = ['Группа L-P', 'Группа F', 'Группа A', 'Группа R'];
        $scope.discriminantSeries = ['Качественное значение'];
        $scope.discriminantData = [
            [1, 1, 1, 1],
            [1, 1, 1, 1]
        ];
        
        function getCompanies() {
            $http
                .get('/get_all_companies')
                .success(success)
                .error(error);

            function success(data) {
                $scope.companies = data;
            }
            function error() {
                console.log('Error when get companies data');
            }
        }

        $scope.selectCompany = (item) => {
            $scope.selectedCompany = item;
            $scope.companySelected = true;
            getAllMeterings();
        };

        function getAllMeterings() {
            $http
                .post('/get_all_meterings',{id:$scope.selectedCompany._id})
                .success(success)
                .error(error);

            function success(data) {
                $scope.meterings = data;
            }
            function error() {
                console.log('Error when get meterings data');
            }
        }
        
        $scope.selectYear = (item) => {
            $scope.selectedYear = item.year;
            $scope.selectedMetering = item;
            defaultMeter = item;
            $scope.readyToStartAnalisis = true;
            $scope.yearSelected = true;
            doRatioAnalysis($scope.selectedMetering);
        };

        //возвращает среднее арифметическое массива
        function avgValOfArray(arr) {
            let arrLen = arr.length, result = 0;
            for (let i in arr) {
                result += arr[i];
            }
            return result / arrLen
        }

        //транспонирует матрицу
        function transMatrix(A) {
            var m = A.length, n = A[0].length, AT = [];
            for (var i = 0; i < n; i++) { AT[i] = [];
                for (var j = 0; j < m; j++) AT[i][j] = A[j][i];
            }
            return AT;
        }

        //ищет максимальный элемент массива
        function indexOfMax(array) {
            let max = array[0];
            let indexOfMax = 0;
            for (let i in array) {
                if (array[i]>max) {
                    max = array[i];
                    indexOfMax = i;
                }
            }
            return indexOfMax;
        }
        
        //определяет качественное значение числа
        //asc - если true - то инверсный ход qualitativeValueOfCoefficient
        function gradeValue(coefficient, vl, l, m, h, vh, asc) {
            let eps = 1e-5;
            if (coefficient < vl || Math.abs(coefficient - vl) < eps) {
                if (asc) {
                    return 'VH'
                } else {
                    return 'VL'
                }
            } else {
                if (coefficient < l || Math.abs(coefficient - l) < eps) {
                    if (asc) {
                        return 'H'
                    } else {
                        return 'L'
                    }
                } else {
                    if (coefficient < m || Math.abs(coefficient - m) < eps) {
                        return 'M'
                    } else {
                        if (coefficient < h || Math.abs(coefficient - h) < eps) {
                            if (asc) {
                                return 'L'
                            } else {
                                return 'H'
                            }
                        } else {
                            if (asc) {
                                return 'VL'
                            } else {
                                return 'VH'
                            }
                        }
                    }
                }
            }
        }

        //определяет количественное значение числа
        function qualitativeValue(coefficient, vl, l, m, h, vh) {
            if (coefficient==='VL') {
                return vl;
            } else {
                if (coefficient==='L') {
                    return l;
                } else {
                    if (coefficient==='M') {
                        return m;
                    } else {
                        if (coefficient==='H') {
                            return h;
                        } else {
                            if (coefficient==='VH') {
                                return vh;
                            }
                        }
                    }
                }
            }
        }

        //интегральное для группы
        function integralValForGroup(group, vl, l, m, h, vh) {
            let  qualitativeValuesForGroup = [];
            for (let i in group) {
                qualitativeValuesForGroup[i] = qualitativeValue(group[i], vl, l, m, h, vh);
            }
            return avgValOfArray(qualitativeValuesForGroup);
        }
        
        //Anya do it! Yeeeah, Anna is super!!
        function multiplyForDiscrim(groupMatrix, groupConstArray, coefficientsArray){
            // let multiplyOfCoef = [];
            // console.log('Число столбцов: ' + groupMatrix[0].length-1);
            // console.log('Число строк: ' + groupMatrix.length-1);
            // for (let i=0; groupMatrix[i].length-1; i++){
            //     var S=0;
            //     for (let j=0; groupMatrix.length-1; j++){
            //         S=S+groupMatrix[i][j]*coefficientsArray[i];
            //         console.log(S);
            //     }
            //     multiplyOfCoef.push(S+groupConstArray[i]);
            // }
            // return multiplyOfCoef;
            let resultArray = [];
            groupMatrix = transMatrix(groupMatrix);
            for (let i in groupMatrix) {
                // console.log('Y');
                // console.log(groupMatrix[i]);
                // console.log('Умножаем на это');
                // console.log(coefficientsArray);
                // console.log('прибавляем это');
                // console.log(groupConstArray);
                // console.log('--------------');
                let result = 0;
                for (let j in groupMatrix[i]) {
                    result += groupMatrix [i][j]*coefficientsArray[j];
                }
                result += groupConstArray[i];
                resultArray.push(result);
            }
            return resultArray;
        }

        //определяет качественное значение для результата дискриминантного анализа
        function getGradeValDA(val) {
            val = parseInt(val);
            switch (val) {
                case 0 : {
                    return 'Очень низкий';
                }
                case 1 : {
                    return 'Низкий';
                }
                case 2 : {
                    return 'Средний';
                }
                case 3 : {
                    return 'Высокий';
                }
                case 4 : {
                    return 'Очень высокий';
                }
                default: {
                    break;
                }
            }
        }

        //коэффициентный анализ
        function doRatioAnalysis(meter) {

            if (meter.oldBalance) {
                //вытаскиваем нужные статьи
                let balanceVal190,
                    balanceVal210, balanceVal220, balanceVal230, balanceVal240, balanceVal244, balanceVal290,
                    balanceVal300,
                    balanceVal450, balanceVal490,
                    balanceVal590,
                    balanceVal610, balanceVal620, balanceVal640, balanceVal690;
                for (let i in meter.balanceValues) {
                    if (meter.balanceValues[i].number === '190') {
                        balanceVal190 = meter.balanceValues[i].val;
                    }

                    if (meter.balanceValues[i].number === '210') {
                        balanceVal210 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '220') {
                        balanceVal220 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '230') {
                        balanceVal230 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '240') {
                        balanceVal240 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '244') {
                        balanceVal244 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '290') {
                        balanceVal290 = meter.balanceValues[i].val;
                    }


                    if (meter.balanceValues[i].number === '300') {
                        balanceVal300 = meter.balanceValues[i].val;
                    }


                    if (meter.balanceValues[i].number === '450') {
                        balanceVal450 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '490') {
                        balanceVal490 = meter.balanceValues[i].val;
                    }


                    if (meter.balanceValues[i].number === '590') {
                        balanceVal590 = meter.balanceValues[i].val;
                    }


                    if (meter.balanceValues[i].number === '610') {
                        balanceVal610 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '620') {
                        balanceVal620 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '640') {
                        balanceVal640 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '690') {
                        balanceVal690 = meter.balanceValues[i].val;
                    }
                }

                let gainsVal010, gainsVal020, gainsVal030, gainsVal040, gainsVal050,
                    gainsVal120, gainsVal140, gainsVal150, gainsVal190;
                for (let i in meter.gainsValues) {
                    if (meter.gainsValues[i].number === '010') {
                        gainsVal010 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '020') {
                        gainsVal020 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '030') {
                        gainsVal030 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '040') {
                        gainsVal040 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '050') {
                        gainsVal050 = meter.gainsValues[i].val;
                    }


                    if (meter.gainsValues[i].number === '120') {
                        gainsVal120 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '140') {
                        gainsVal140 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '150') {
                        gainsVal150 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '190') {
                        gainsVal190 = meter.gainsValues[i].val;
                    }
                }

                //первая группа - L-P
                $scope.l1 = (balanceVal290 - balanceVal210 - balanceVal220 - balanceVal230)/(balanceVal690 - balanceVal640);
                $scope.l1GradeVal = gradeValue($scope.l1, 0.1, 0.5, 1, 1.8, 3, false);

                $scope.l3 = (balanceVal290 - balanceVal590 - balanceVal690 + balanceVal640 + balanceVal610)/balanceVal210*100;
                $scope.l3GradeVal = gradeValue($scope.l3, 60, 127, 220, 407, 600, false);

                $scope.p1 = (balanceVal290-balanceVal230)/(balanceVal690 - balanceVal640);
                $scope.p1GradeVal = gradeValue($scope.p1, 0.5, 0.7, 0.9, 1.3, 3, false);

                $scope.lpIntegral = integralValForGroup([$scope.l1GradeVal,$scope.l3GradeVal,$scope.p1GradeVal],0.1, 0.3, 0.5, 0.7, 0.9);
                $scope.lpIntegralGradeVal = gradeValue($scope.lpIntegral, 0.1, 0.3, 0.5, 0.7, 0.9);

                //вторая группа - F1-F4
                $scope.f1 = (balanceVal590 + balanceVal690 - balanceVal640)/(balanceVal490 - balanceVal450 + balanceVal640);
                $scope.f1GradeVal = gradeValue($scope.f1, 0.5, 1.03, 1.5, 2.3, 4, true);

                $scope.f2 = (balanceVal490 - balanceVal450 + balanceVal640)/(balanceVal190 + balanceVal290);
                $scope.f2GradeVal = gradeValue($scope.f2, 0.25, 0.48, 0.8, 0.9, 1, false);

                $scope.f3 = (balanceVal490 - balanceVal450 + balanceVal640 -balanceVal190)/balanceVal210;
                $scope.f3GradeVal = gradeValue($scope.f3, -2, -0.67, 0.7, 1.57, 4, false);

                $scope.f4 = (balanceVal190 + balanceVal230)/(balanceVal490 - balanceVal450 + balanceVal640);
                $scope.f4GradeVal = gradeValue($scope.f4, 0.5, 0.77, .1, 1.43, 3, true);

                $scope.fIntegral = integralValForGroup([$scope.f1GradeVal,$scope.f2GradeVal,$scope.f3GradeVal,$scope.f4GradeVal],0.1, 0.3, 0.5, 0.7, 0.9);
                $scope.fIntegralGradeVal = gradeValue($scope.fIntegral, 0.1, 0.3, 0.5, 0.7, 0.9);

                //третья группа - A
                $scope.a2 = gainsVal010/balanceVal300;
                $scope.a2GradeVal = gradeValue($scope.a2, 0.06, 0.13, 0.22, 0.27, 1, false);

                $scope.a4 = (gainsVal010 - gainsVal030 - gainsVal040)/balanceVal620;
                $scope.a4GradeVal = gradeValue($scope.a4, 0.4, 0.8, 1.4, 1.93, 10, false);

                $scope.a5 = gainsVal010/(balanceVal230 + balanceVal240 - balanceVal244);
                $scope.a5GradeVal = gradeValue($scope.a5, 0.6, 1, 1.6, 2.13, 10, false);

                $scope.a6 = Math.abs(gainsVal020/balanceVal210);
                $scope.a6GradeVal = gradeValue($scope.a6, 1, 2, 3, 5, 10, false);

                $scope.aIntegral = integralValForGroup([$scope.a2GradeVal,$scope.a4GradeVal,$scope.a5GradeVal,$scope.a6GradeVal],0.1, 0.3, 0.5, 0.7, 0.9);
                $scope.aIntegralGradeVal = gradeValue($scope.aIntegral, 0.1, 0.3, 0.5, 0.7, 0.9);

                //четвертая группа - R1-R4
                $scope.r1 = (gainsVal140 - gainsVal150)/(gainsVal010 + gainsVal120)*100;
                $scope.r1GradeVal = gradeValue($scope.r1, 0, 4, 12, 24, 70, false);

                $scope.r2 = gainsVal190/balanceVal300*100;
                $scope.r2GradeVal = gradeValue($scope.r2, -3, -1, 1, 3.67, 6, false);

                $scope.r3 = gainsVal190/(balanceVal490 - balanceVal450 + balanceVal640)*100;
                $scope.r3GradeVal = gradeValue($scope.r3, -4, -1.33, 2, 6.67, 20, false);

                $scope.r4 = gainsVal050/gainsVal010*100;
                $scope.r4GradeVal = gradeValue($scope.r4, 0, 4, 12, 24, 50, false);

                $scope.rIntegral = integralValForGroup([$scope.r1GradeVal,$scope.r2GradeVal,$scope.r3GradeVal,$scope.r4GradeVal],0.1, 0.3, 0.5, 0.7, 0.9);
                $scope.rIntegralGradeVal = gradeValue($scope.rIntegral, 0.1, 0.3, 0.5, 0.7, 0.9);

                //итоговый показатель
                $scope.unionIntegral = avgValOfArray([$scope.lpIntegral,$scope.fIntegral,$scope.aIntegral,$scope.rIntegral]);
                $scope.unionIntegralGradeVal = gradeValue($scope.unionIntegral, 0.2, 0.4, 0.6, 0.8, 0.81);
            } else {
                //вытаскиваем нужные статьи нового баланса
                let balanceVal1100,
                    balanceVal1200, balanceVal1210, balanceVal1220, balanceVal1230,
                    balanceVal1300,
                    balanceVal1400,
                    balanceVal1500, balanceVal1510, balanceVal1520, balanceVal15207, balanceVal1530,
                    balanceVal1600;

                for (let i in meter.balanceValues) {
                    if (meter.balanceValues[i].number === '1100') {
                        balanceVal1100 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1200') {
                        balanceVal1200 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1210') {
                        balanceVal1210 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1220') {
                        balanceVal1220 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1230') {
                        balanceVal1230 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1300') {
                        balanceVal1300 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1400') {
                        balanceVal1400 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1500') {
                        balanceVal1500 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1510') {
                        balanceVal1510 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1520') {
                        balanceVal1520 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '15207') {
                        balanceVal15207 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1530') {
                        balanceVal1530 = meter.balanceValues[i].val;
                    }
                    if (meter.balanceValues[i].number === '1600') {
                        balanceVal1600 = meter.balanceValues[i].val;
                    }
                }

                let gainsVal2110, gainsVal2120,
                    gainsVal2200, gainsVal2210, gainsVal2220,
                    gainsVal2300, gainsVal2340,
                    gainsVal2400, gainsVal2410;

                for (let i in meter.gainsValues) {
                    if (meter.gainsValues[i].number === '2110') {
                        gainsVal2110 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '2120') {
                        gainsVal2120 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '2200') {
                        gainsVal2200 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '2210') {
                        gainsVal2210 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '2220') {
                        gainsVal2220 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '2300') {
                        gainsVal2300 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '2340') {
                        gainsVal2340 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '2400') {
                        gainsVal2400 = meter.gainsValues[i].val;
                    }
                    if (meter.gainsValues[i].number === '2410') {
                        gainsVal2410 = meter.gainsValues[i].val;
                    }
                }

                //первая группа - L-P
                //TODO check it
                $scope.l1 = (balanceVal1200 - balanceVal1210 - balanceVal1220 - balanceVal1230)/(balanceVal1500 - balanceVal1530);
                $scope.l1GradeVal = gradeValue($scope.l1, 0.1, 0.5, 1, 1.8, 3, false);

                $scope.l3 = (balanceVal1200 - balanceVal1400 - balanceVal1500 + balanceVal1530 + balanceVal1510 - balanceVal1230)/balanceVal1210*100;
                $scope.l3GradeVal = gradeValue($scope.l3, 60, 127, 220, 407, 600, false);

                //TODO check it
                $scope.p1 = (balanceVal1200 - balanceVal1230)/(balanceVal1500 - balanceVal1530);
                $scope.p1GradeVal = gradeValue($scope.p1, 0.5, 0.7, 0.9, 1.3, 3, false);

                $scope.lpIntegral = integralValForGroup([$scope.l1GradeVal,$scope.l3GradeVal,$scope.p1GradeVal],0.1, 0.3, 0.5, 0.7, 0.9);
                $scope.lpIntegralGradeVal = gradeValue($scope.lpIntegral, 0.1, 0.3, 0.5, 0.7, 0.9);

                //вторая группа - F1-F4
                $scope.f1 = (balanceVal1400 + balanceVal1500 - balanceVal1530)/(balanceVal1300 + balanceVal1530);
                $scope.f1GradeVal = gradeValue($scope.f1, 0.5, 1.03, 1.5, 2.3, 4, true);

                $scope.f2 = (balanceVal1300 + balanceVal1530)/(balanceVal1100 + balanceVal1200);
                $scope.f2GradeVal = gradeValue($scope.f2, 0.25, 0.48, 0.8, 0.9, 1, false);

                $scope.f3 = (balanceVal1300 + balanceVal1530 - balanceVal1100)/balanceVal1210;
                $scope.f3GradeVal = gradeValue($scope.f3, -2, -0.67, 0.7, 1.57, 4, false);

                $scope.f4 = (balanceVal1100 + balanceVal1230)/(balanceVal1300 + balanceVal1530);
                $scope.f4GradeVal = gradeValue($scope.f4, 0.5, 0.77, .1, 1.43, 3, true);

                $scope.fIntegral = integralValForGroup([$scope.f1GradeVal,$scope.f2GradeVal,$scope.f3GradeVal,$scope.f4GradeVal],0.1, 0.3, 0.5, 0.7, 0.9);
                $scope.fIntegralGradeVal = gradeValue($scope.fIntegral, 0.1, 0.3, 0.5, 0.7, 0.9);

                //третья группа - A
                $scope.a2 = gainsVal2110/balanceVal1600;
                $scope.a2GradeVal = gradeValue($scope.a2, 0.06, 0.13, 0.22, 0.27, 1, false);

                $scope.a4 = (gainsVal2110 - gainsVal2210 - gainsVal2220)/balanceVal1520;
                $scope.a4GradeVal = gradeValue($scope.a4, 0.4, 0.8, 1.4, 1.93, 10, false);

                $scope.a5 = gainsVal2110/(balanceVal1230);
                $scope.a5GradeVal = gradeValue($scope.a5, 0.6, 1, 1.6, 2.13, 10, false);

                $scope.a6 = Math.abs(gainsVal2120/balanceVal1210);
                $scope.a6GradeVal = gradeValue($scope.a6, 1, 2, 3, 5, 10, false);

                $scope.aIntegral = integralValForGroup([$scope.a2GradeVal,$scope.a4GradeVal,$scope.a5GradeVal,$scope.a6GradeVal],0.1, 0.3, 0.5, 0.7, 0.9);
                $scope.aIntegralGradeVal = gradeValue($scope.aIntegral, 0.1, 0.3, 0.5, 0.7, 0.9);

                //четвертая группа - R1-R4
                $scope.r1 = (gainsVal2300 - gainsVal2410)/(gainsVal2110 + gainsVal2340)*100;
                $scope.r1GradeVal = gradeValue($scope.r1, 0, 4, 12, 24, 70, false);

                $scope.r2 = gainsVal2400/balanceVal1600*100;
                $scope.r2GradeVal = gradeValue($scope.r2, -3, -1, 1, 3.67, 6, false);

                $scope.r3 = gainsVal2400/(balanceVal1300 + balanceVal1530)*100;
                $scope.r3GradeVal = gradeValue($scope.r3, -4, -1.33, 2, 6.67, 20, false);

                $scope.r4 = gainsVal2200/gainsVal2110*100;
                $scope.r4GradeVal = gradeValue($scope.r4, 0, 4, 12, 24, 50, false);

                $scope.rIntegral = integralValForGroup([$scope.r1GradeVal,$scope.r2GradeVal,$scope.r3GradeVal,$scope.r4GradeVal],0.1, 0.3, 0.5, 0.7, 0.9);
                $scope.rIntegralGradeVal = gradeValue($scope.rIntegral, 0.1, 0.3, 0.5, 0.7, 0.9);

                //итоговый показатель
                $scope.unionIntegral = avgValOfArray([$scope.lpIntegral,$scope.fIntegral,$scope.aIntegral,$scope.rIntegral]);
                $scope.unionIntegralGradeVal = gradeValue($scope.unionIntegral, 0.2, 0.4, 0.6, 0.8, 0.81);
            }

            doRatioChart();
            doDiscriminantAnalysis($scope.selectedMetering);
        }

        function doDiscriminantAnalysis(meter) {
            $scope.lpValues = multiplyForDiscrim(yLP,yLPConst,[$scope.l1, $scope.l3, $scope.p1]);
            $scope.lpResult  = getGradeValDA(indexOfMax($scope.lpValues));

            $scope.fValues = multiplyForDiscrim(yF,yFConst,[$scope.f1, $scope.f2, $scope.f3, $scope.f4]);
            $scope.fResult  = getGradeValDA(indexOfMax($scope.fValues));

            $scope.aValues = multiplyForDiscrim(yA,yAConst,[$scope.a2, $scope.a4, $scope.a5, $scope.a6]);
            $scope.aResult  = getGradeValDA(indexOfMax($scope.aValues));

            $scope.rValues = multiplyForDiscrim(yR,yRConst,[$scope.r1, $scope.r2, $scope.r3, $scope.r4]);
            $scope.rResult  = getGradeValDA(indexOfMax($scope.rValues));

            $scope.allValues = multiplyForDiscrim(yAll,yAllConst,[$scope.l1, $scope.l3, $scope.p1, $scope.f1, $scope.f2, $scope.f3, $scope.f4, $scope.a2, $scope.a4, $scope.a5, $scope.a6, $scope.r1, $scope.r2, $scope.r3, $scope.r4]);
            $scope.allResult  = getGradeValDA(indexOfMax($scope.allValues));

            doDiscriminantChart();
        }

        function setDiscriminantValToChart(val,arrayToChart,pos) {
            switch (val) {
                case 'Очень низкий': {
                    arrayToChart[pos] = 1;
                    break;
                }
                case 'Низкий': {
                    arrayToChart[pos] = 2;
                    break;
                }
                case 'Средний': {
                    arrayToChart[pos] = 3;
                    break;
                }
                case 'Высокий': {
                    arrayToChart[pos] = 4;
                    break;
                }
                case 'Очень высокий': {
                    arrayToChart[pos] = 5;
                    break;
                }
            }
        }

        function setRatioValToChart(val,arrayToChart,pos) {
            switch (val) {
                case 'VL': {
                    arrayToChart[pos] = 0.5;
                    break;
                }
                case 'L': {
                    arrayToChart[pos] = 1;
                    break;
                }
                case 'M': {
                    arrayToChart[pos] = 1.5;
                    break;
                }
                case 'H': {
                    arrayToChart[pos] = 2;
                    break;
                }
                case 'VH': {
                    arrayToChart[pos] = 2.5;
                    break;
                }
            }
        }
        
        function doRatioChart() {
            let currentRatioValues = [0,0,0,0];
            setRatioValToChart($scope.lpIntegralGradeVal,currentRatioValues,0);
            setRatioValToChart($scope.fIntegralGradeVal,currentRatioValues,1);
            setRatioValToChart($scope.aIntegralGradeVal,currentRatioValues,2);
            setRatioValToChart($scope.rIntegralGradeVal,currentRatioValues,3);

            $scope.ratioData = [
                [$scope.lpIntegral.toFixed(3), $scope.fIntegral.toFixed(3), $scope.aIntegral.toFixed(3), $scope.rIntegral.toFixed(3)],
                currentRatioValues
            ];
        }

        function doDiscriminantChart() {
            let currentDiscriminantValues = [0,0,0,0];
            setDiscriminantValToChart($scope.lpResult,currentDiscriminantValues,0);
            setDiscriminantValToChart($scope.fResult,currentDiscriminantValues,1);
            setDiscriminantValToChart($scope.aResult,currentDiscriminantValues,2);
            setDiscriminantValToChart($scope.rResult,currentDiscriminantValues,3);

            //setRatioValToChart($scope.lpIntegralGradeVal,currentRatioValues,0);
            $scope.discriminantData = [
                currentDiscriminantValues
            ];
        }
        
        $scope.resetMeterToDefault = () => {
            //TODO fix this
            $scope.selectedMetering.balanceValues = defaultMeter.balanceValues;
            $scope.selectedMetering.gainsValues = defaultMeter.gainsValues;
        };
        
        //watchers
        $scope.$watch('selectedMetering',(val) => {
            if (val) {
                doRatioAnalysis($scope.selectedMetering);
            }
        },true);
        
        //start
        getCompanies();
    }
);