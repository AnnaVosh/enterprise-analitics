var moment_module = angular.module('moment_module', []);
moment_module.factory('moment', ['$window', function($window) {
    return $window.moment;
}]);